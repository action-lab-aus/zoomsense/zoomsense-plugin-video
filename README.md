# Zoomsense Plugin - Video

## Configuration

```js
{
  src: ""(required);
  muted: true | false(optional);
  volume: 0 - 100(optional);
  loop: optional;
}
```

## Status

```js
{
  time: time in seconds since start
  duration: total duration in seconds
  status: "loading" or "ready"
}
```
