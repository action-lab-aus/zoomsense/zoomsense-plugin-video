module.exports = {
  css: {
    extract: false,
  },
  configureWebpack: {
    // externals: [/^quasar\/.+$/, /^firebase\/.+$/, /^core-js\/.+$/],
    externals: [
      // /^quasar\/.+$/,
      /^firebase\/.+$/,
      /^vuefire\/.+$/,
      // "core-js",
      // "@quasar",
      // /\bcore-js\b/,
      // /\bwebpack\/buildin\b/,
    ],
  },

  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: false,
    },
  },

  transpileDependencies: ["quasar"],
};
